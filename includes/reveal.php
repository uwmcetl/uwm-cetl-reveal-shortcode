<?php
// Block non-WP loading
if ( !defined( 'ABSPATH' ) ) // Or some other WordPress constant
     exit;

/**
 * Shortcode method to accept the ID of a post and show the wrapped contents, 
 * or optionall hide the contents.
 *
 * $atts 		Array of shortcode attributes
 * $content		String of contents of shortcode wrapping
 */
function reveal_shortcode( $atts, $content = null ) {
	$SHOW_CONTENT = "show";
	$HIDE_CONTENT = "hide";
	
	// $atts defaults
	$clean_atts = shortcode_atts( array(
		'list'	=> "",
		'type'	=> $SHOW_CONTENT
	), $atts );
	
	// gather info to start
	$response = $content;
	$search_user = strtolower( $_SERVER["REMOTE_USER"] );
	
	// get the post if a valid user exists
	if ( !empty( $search_user ) ) {
		$target_post = get_post( $clean_atts["list"] );
	} else {
		$target_post = NULL;
	}
	
	
	// if the post is valid, evaluate
	if ( $target_post != NULL && isset( $target_post->post_content ) ) {

		// if an array somehow is returned, take the first post
		if ( is_array( $target_post ) ) {
			$target_post = $target_post[0];
		}
		
		// search the content of the passed post
		// Remember that the variable is good for $SHOW_CONTENT by default
		// so we only need to zero out $response if needed
		$is_valid_user = strpos( strtolower( $target_post->post_content) , $search_user );
		if ( $is_valid_user === FALSE ) {
			
			// do the oppiset of whatever was called in the shortcode
			if ( $clean_atts['type'] === $SHOW_CONTENT ) {
				$response = "";
			}
			
		} else {
			
			// do the same as whatever was called for in the shortcode
			if ( $clean_atts['type'] === $HIDE_CONTENT ) {
				$response = "";
			}
			
		}
		
	}

	return do_shortcode( $response );
}

add_shortcode("reveal", "reveal_shortcode");